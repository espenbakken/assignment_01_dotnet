﻿using Assignment01_RPGHeroes.HeroAttributes;
using Assignment01_RPGHeroes.Enums;
using Assignment01_RPGHeroes.Exceptions;
using Assignment01_RPGHeroes.Items;

namespace Assignment01_RPGHeroes.Heroes
{
    public class Rogue : Hero
    {
        //Constructor
        public Rogue(string name) : base(name, HeroAttributeCreator.RogueStartingAttributes(), new ArmorType[] {ArmorType.Leather, ArmorType.Mail},
            new WeaponType[] { WeaponType.Daggers, WeaponType.Swords }) {}
        
        //Methods
        public override void LevelUp()
        {
            Level += 1;
            BaseStatistics += HeroAttributeCreator.RogueLevelUp();
        }

        public void CalcTotalAttr(Rogue rogue)
        {
            TotalStatistics = BaseStatistics;
            foreach (var item in rogue.Inventory)
            {
                try
                {
                    if (item.Value.GetType() == typeof(Armor))
                    {
                        Armor? armor = item.Value as Armor;
                        TotalStatistics += armor!.Attributes;
                    }
                }
                catch (AttributeException)
                {
                    throw new AttributeException("Cannot calculate attributes");
                }
            }
        }

        public double CalcDmg(Rogue rogue)
        {
            try
            {
                if (rogue.Inventory.ContainsKey(Slot.Weapon) == false) return Dmg;

                if (rogue.Inventory.ContainsKey(Slot.Weapon) != false)
                {
                    Weapon weapon = (Weapon)Inventory[Slot.Weapon];
                    double damage = (double)(weapon.Damage * (1 + BaseStatistics.Dexterity / 100));
                    return damage;
                }
                return Dmg;
            }
            catch (InvalidWeaponException)
            {
                throw new InvalidWeaponException("Cannot calculate the damage with the weapon.");
            }
        }
    }
}

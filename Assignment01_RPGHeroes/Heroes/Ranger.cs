﻿using Assignment01_RPGHeroes.HeroAttributes;
using Assignment01_RPGHeroes.Enums;
using Assignment01_RPGHeroes.Exceptions;
using Assignment01_RPGHeroes.Items;

namespace Assignment01_RPGHeroes.Heroes
{
    public class Ranger : Hero
    {
        //Constructor
        public Ranger(string name) : base(name, HeroAttributeCreator.RangerStartingAttributes(), new ArmorType[] { ArmorType.Leather, ArmorType.Mail },
            new WeaponType[] { WeaponType.Bows }) {}


        //Methods
        public override void LevelUp()
        {
            Level += 1;
            BaseStatistics += HeroAttributeCreator.RangerLevelUp();
        }

        public void CalcTotalAttr(Ranger ranger)
        {
            TotalStatistics = BaseStatistics;
            foreach (var item in ranger.Inventory)
            {
                try
                {
                    if (item.Value.GetType() == typeof(Armor))
                    {
                        Armor? armor = item.Value as Armor;
                        TotalStatistics += armor!.Attributes;
                    }
                }
                catch (AttributeException)
                {
                    throw new AttributeException("Cannot calculate attributes");
                }
            }
        }

        public double CalcDmg(Ranger ranger)
        { 
            try
            {
                if (ranger.Inventory.ContainsKey(Slot.Weapon) == false) return Dmg;

                if (ranger.Inventory.ContainsKey(Slot.Weapon) != false)
                {
                    Weapon weapon = (Weapon)Inventory[Slot.Weapon];
                    double damage = (double)(weapon.Damage * (1 + BaseStatistics.Dexterity / 100));
                    return damage;
                }
                return Dmg;
            }
            catch (InvalidWeaponException)
            {
                throw new InvalidWeaponException("Cannot calculate the damage with the weapon.");
            }
        }
    }
}

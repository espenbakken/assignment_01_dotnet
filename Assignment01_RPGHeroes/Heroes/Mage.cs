﻿using Assignment01_RPGHeroes.Enums;
using Assignment01_RPGHeroes.Exceptions;
using Assignment01_RPGHeroes.HeroAttributes;
using Assignment01_RPGHeroes.Items;

namespace Assignment01_RPGHeroes.Heroes
{
    public class Mage : Hero
    {
        //Constructor
        public Mage(string name) : base(name, HeroAttributeCreator.MageStartingAttributes(), new ArmorType[] { ArmorType.Cloth },
                new WeaponType[] { WeaponType.Staffs, WeaponType.Wand}) { }

       //Methods
       public override void LevelUp()
        {
            Level += 1;
            BaseStatistics += HeroAttributeCreator.MageLevelUp();
        }

        public void CalcTotalAttr(Mage mage)
        {
            TotalStatistics = BaseStatistics;
            foreach (var item in mage.Inventory)
            {
                try
                {
                    if(item.Value.GetType() == typeof(Armor))
                    {
                        Armor? armor = item.Value as Armor;
                        TotalStatistics += armor!.Attributes;
                    }
                }
                catch (AttributeException)
                {
                    throw new AttributeException("Cannot calculate attributes");
                }
            }
        }

        public double CalcDmg(Mage mage)
        {
            try
            {
                if (mage.Inventory.ContainsKey(Slot.Weapon) == false) return Dmg;

                if(mage.Inventory.ContainsKey(Slot.Weapon) != false)
                {
                    Weapon weapon = (Weapon)Inventory[Slot.Weapon];
                    double damage = (double)(weapon.Damage * (1 + BaseStatistics.Intelligence / 100));
                    return damage;
                }
                return Dmg;
            }
            catch (InvalidWeaponException)
            {
                throw new InvalidWeaponException("Cannot calculate the damage with the weapon.");
            }
        }
    }
}

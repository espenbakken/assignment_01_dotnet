﻿using Assignment01_RPGHeroes.HeroAttributes;
using Assignment01_RPGHeroes.Enums;
using Assignment01_RPGHeroes.Exceptions;
using Assignment01_RPGHeroes.Items;

namespace Assignment01_RPGHeroes.Heroes
{
    public class Warrior : Hero
    {
        //Constructor
        public Warrior(string name) : base(name, HeroAttributeCreator.WarriorStartingAttributes(), new ArmorType[] { ArmorType.Mail, ArmorType.Plate }, 
            new WeaponType[] { WeaponType.Wand }){}

        //Methods
        public override void LevelUp()
        {
            Level += 1;
            BaseStatistics += HeroAttributeCreator.WarriorLevelUp();
        }

        public void CalcTotalAttr(Warrior warrior)
        {
            TotalStatistics = BaseStatistics;
            foreach (var item in warrior.Inventory)
            {
                try
                {
                    if (item.Value.GetType() == typeof(Armor))
                    {
                        Armor? armor = item.Value as Armor;
                        TotalStatistics += armor!.Attributes;
                    }
                }
                catch (AttributeException)
                {
                    throw new AttributeException("Cannot calculate attributes");
                }
            }
        }

        public double CalcDmg(Warrior warrior)
        {
            try
            {
                if (warrior.Inventory.ContainsKey(Slot.Weapon) == false) return Dmg;

                if (warrior.Inventory.ContainsKey(Slot.Weapon) != false)
                {
                    Weapon weapon = (Weapon)Inventory[Slot.Weapon];
                    double damage = (double)(weapon.Damage * (1 + BaseStatistics.Strength / 100));
                    return damage;
                }
                return Dmg;
            }
            catch (InvalidWeaponException)
            {
                throw new InvalidWeaponException("Cannot calculate the damage with the weapon.");
            }
        }
    }
}

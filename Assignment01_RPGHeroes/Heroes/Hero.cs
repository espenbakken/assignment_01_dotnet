﻿using Assignment01_RPGHeroes.HeroAttributes;
using Assignment01_RPGHeroes.Enums;
using Assignment01_RPGHeroes.Items;
using Assignment01_RPGHeroes.Exceptions;

namespace Assignment01_RPGHeroes.Heroes
{
    public abstract class Hero
    {
        //Attributes
        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public double Dmg { get; set; } = 1;
        public ArmorType[]? AccArmors { get; set; }
        public WeaponType[]? AccWeapons {get; set; }
        public HeroAttribute BaseStatistics { get; set; }
        public HeroAttribute? TotalStatistics { get; set; }
        public Dictionary<Slot, Item> Inventory { get; set; } = new();
       
        //Constructor
        public Hero(string name, HeroAttribute baseStatistics, ArmorType[] accArmors, WeaponType[] accWeapons )
        {
            Name = name;
            BaseStatistics = baseStatistics;
            AccArmors = accArmors;
            AccWeapons = accWeapons;
        }

        //Methods
        public abstract void LevelUp();

        public void EquipItem(Item item)
        {
            if(Level >= item.GetLevel())
            {
                if (Inventory.ContainsKey(item.GetSlot()) == true) Inventory[item.GetSlot()] = item;
                if (Inventory.ContainsKey(item.GetSlot()) == false) Inventory.Add(item.GetSlot(), item);
            } 
            else
            {
                throw new TooLowLevelException("Your level is too low to equip this item!");
            }
            
        }

        public string EquipWeapon(Weapon item)
        {
            if (AccWeapons!.Contains(item.GetWeaponType()))
            {
                EquipItem(item);
                return "New weapon equipped!";
            }
            else
            {
                throw new InvalidWeaponException("Can't equip this weapon!");
            }
        }

        public string EquipArmor(Armor item)
        {
           if (AccArmors!.Contains(item.GetArmorType()))
            {
                EquipItem(item);
                return "You have equipped this armor!";
            } else
            {
                throw new InvalidArmorException("Can't equip this armour!");
            }
        }

        public HeroAttribute CalcTotalAttr()
        {
            TotalStatistics = BaseStatistics;
            foreach (var item in Inventory)
            {
                if(item.Value.GetType() == typeof(Armor))
                {
                    Armor? armor = item.Value as Armor;
                    TotalStatistics += armor!.Attributes;
                }
            }
            return TotalStatistics;
        }

        public virtual double damage()
        {
            if (Inventory[Slot.Weapon] == null)
            {
                return 1;
            }
            else
            {
                Weapon? weapon = Inventory[Slot.Weapon] as Weapon;
                return weapon.Damage;
            }
        }
    }
}

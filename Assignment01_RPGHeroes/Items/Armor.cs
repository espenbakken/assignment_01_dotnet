﻿using Assignment01_RPGHeroes.HeroAttributes;
using Assignment01_RPGHeroes.Enums;

namespace Assignment01_RPGHeroes.Items
{
    public class Armor : Item
    {
        //Attributes
        public ArmorType ArmorType { get; }
        public HeroAttribute Attributes { get; }

        //Constructor
        public Armor(string name, int requiredLevel, Slot requiredSlot, ArmorType armorType, HeroAttribute attributes)
            : base(name, requiredLevel, requiredSlot)
        {
            ArmorType = armorType;
            Attributes = attributes;
        }

        //Methods
        public bool Equals(Armor checker) => Name == checker.Name &&
                                           RequiredLevel == checker.RequiredLevel &&
                                           RequiredSlot == checker.RequiredSlot &&
                                           ArmorType == checker.ArmorType &&
                                           Attributes.Equals(checker.Attributes);
        
        public ArmorType GetArmorType() => ArmorType;
    }
}

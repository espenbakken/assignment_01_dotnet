﻿using Assignment01_RPGHeroes.Enums;

namespace Assignment01_RPGHeroes.Items
{
    public class Weapon : Item
    {
        //Attributes
        public int Damage { get; set; }
        public WeaponType Type { get; }

        //Constructor
        public Weapon(string name, int requiredLevel, int damage, WeaponType type)
            : base(name, requiredLevel, Slot.Weapon)
        {
            Damage = damage;
            Type = type;
        }

        //Methods
        public bool Equals(Weapon checker) => Name == checker.Name &&
                                              RequiredLevel == checker.RequiredLevel &&
                                              RequiredSlot == checker.RequiredSlot &&
                                              Damage == checker.Damage &&
                                              Type == checker.Type;

        public WeaponType GetWeaponType() => Type;
    }
}

﻿using Assignment01_RPGHeroes.Enums;

namespace Assignment01_RPGHeroes.Items
{
    public class Item 
    {
        //Attributes
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public Slot RequiredSlot { get; set; }

        //Constructor
        public Item(string name, int requiredLevel, Slot requiredSlot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            RequiredSlot = requiredSlot;
        }

        //Methods
        public int GetLevel() => RequiredLevel;

        public Slot GetSlot() => RequiredSlot;
    }
}

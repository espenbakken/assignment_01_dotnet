﻿using Assignment01_RPGHeroes.Enums;
using Assignment01_RPGHeroes.Heroes;
using Assignment01_RPGHeroes.Items;
using Assignment01_RPGHeroes.HeroAttributes;
using System.Text;

namespace Assignment01_RPGHeroes.Main
{
    public class Display
    {
        public static bool MainMenu()
        {
            Console.WriteLine("What do you want to see? Type in the number and press enter.");
            Console.WriteLine("Armours (1)");
            Console.WriteLine("Weapons (2)");
            Console.WriteLine("Heroes (3)");
            Console.WriteLine("Quit Program (0)");
            Console.Write("\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "0":
                    Console.WriteLine("Thanks for playing!\n");
                    return false;
                case "1":
                    DisplayArmours();
                    return true;
                case "2":
                    DisplayWeapons();
                    return true;
                case "3":
                    DisplayHeroes();
                    return true;
                default:
                    return true;
            }
        }
        /**Function to display all created heroes (Mage, Ranger, Rogue and warrior)
         * Prints out their type, name, level, damage and statistics. */
        public static void DisplayHeroes()
        {
            Console.Clear();
            StringBuilder stringBuilder = new StringBuilder();
            Console.WriteLine("Heroes:");
            Console.WriteLine("--------------------------------------------\n");

            //Mage
            Mage mage = new("Espen");
            mage.LevelUp();
            Weapon weapon = new("Wand", 2, 2, WeaponType.Wand);
            mage.EquipWeapon(weapon);
            mage.CalcTotalAttr(mage);

            stringBuilder.AppendLine($"Mage\n");
            stringBuilder.AppendLine($"Name: {mage.Name}");
            stringBuilder.AppendLine($"Level: {mage.Level}");
            stringBuilder.AppendLine($"Total strength: {mage.BaseStatistics?.Strength}");
            stringBuilder.AppendLine($"Total dexterity: {mage.BaseStatistics?.Dexterity}");
            stringBuilder.AppendLine($"Total intelligence: {mage.BaseStatistics?.Intelligence}");
            stringBuilder.AppendLine($"Damage: {mage.CalcDmg(mage)}");
            
            //Printing all items in inventory
            foreach (Item item in mage.Inventory.Values)
            {
                stringBuilder.AppendLine($"Inventory item: {item.Name}");
            }
            stringBuilder.AppendLine($"----------------------\n");

            //Ranger
            Ranger ranger = new("Kristian");
            ranger.LevelUp();

            stringBuilder.AppendLine($"Ranger\n");
            stringBuilder.AppendLine($"Name: {ranger.Name}");
            stringBuilder.AppendLine($"Level: {ranger.Level}");
            stringBuilder.AppendLine($"Total strength: {ranger.BaseStatistics?.Strength}");
            stringBuilder.AppendLine($"Total intelligence: {ranger.BaseStatistics?.Intelligence}");
            stringBuilder.AppendLine($"Total dexterity: {ranger.BaseStatistics?.Dexterity}");
            stringBuilder.AppendLine($"Damage: {ranger.CalcDmg(ranger)}");
            stringBuilder.AppendLine($"----------------------\n");

            //Rogue
            Rogue rogue = new("Knut med trut");
            rogue.LevelUp();
            rogue.LevelUp();
            Weapon weapon2 = new("Epic sword", 3, 3, WeaponType.Swords);
            rogue.EquipWeapon(weapon2);

            stringBuilder.AppendLine($"Rogue\n");
            stringBuilder.AppendLine($"Name: {rogue.Name}");
            stringBuilder.AppendLine($"Level: {rogue.Level}");
            stringBuilder.AppendLine($"Total strength: {rogue.BaseStatistics?.Strength}");
            stringBuilder.AppendLine($"Total intelligence: {rogue.BaseStatistics?.Intelligence}");
            stringBuilder.AppendLine($"Total dexterity: {rogue.BaseStatistics?.Dexterity}");
            stringBuilder.AppendLine($"Damage: {rogue.CalcDmg(rogue)}");

            //Printing all items in inventory
            foreach (Item item in rogue.Inventory.Values)
            {
                stringBuilder.AppendLine($"Inventory item: {item.Name}");
            }
            stringBuilder.AppendLine($"----------------------\n");

            //Warrior
            Warrior warrior = new("Kristian von crow");
            warrior.LevelUp();

            stringBuilder.AppendLine($"Warrior\n");
            stringBuilder.AppendLine($"Name: {warrior.Name}");
            stringBuilder.AppendLine($"Level: {warrior.Level}");
            stringBuilder.AppendLine($"Total strength: {warrior.BaseStatistics?.Strength}");
            stringBuilder.AppendLine($"Total intelligence: {warrior.BaseStatistics?.Intelligence}");
            stringBuilder.AppendLine($"Total dexterity: {warrior.BaseStatistics?.Dexterity}");
            stringBuilder.AppendLine($"Damage: {warrior.CalcDmg(warrior)}");
            stringBuilder.AppendLine($"----------------------\n");

            Console.WriteLine(stringBuilder);
        }

        /**Function to display created weapons (Battle Axe and Fighting dagger) (If user write 
         * Prints out their name, required levels, damage, and type*/
        public static void DisplayWeapons()
        {
            Console.Clear();
            StringBuilder stringBuilder = new StringBuilder();

            Weapon weaponOne = new("Battle Axe", 1, 1, WeaponType.Axes);
            Weapon weaponTwo = new("Fighting dagger", 2, 4, WeaponType.Daggers);

            Console.WriteLine("Weapons:");
            Console.WriteLine("--------------------------------------------\n");
            // First weapon
            stringBuilder.AppendLine($"Weapon: {weaponOne.Name}");
            stringBuilder.AppendLine($"Required level: {weaponOne.RequiredLevel}");
            stringBuilder.AppendLine($"Damage: {weaponOne.Damage}");
            stringBuilder.AppendLine($"Weapon type: {weaponOne.Type}\n");
            // Second weapon
            stringBuilder.AppendLine($"Weapon: {weaponTwo.Name}");
            stringBuilder.AppendLine($"Required level: {weaponTwo.RequiredLevel}");
            stringBuilder.AppendLine($"Damage: {weaponTwo.Damage}");
            stringBuilder.AppendLine($"Weapon type: {weaponTwo.Type}");
            
            Console.WriteLine(stringBuilder);
        }

        /**Function to display created armors (Pig blanket and Breastplate) 
         * Prints out their name, required levels, slots, armortype and attributes*/
        public static void DisplayArmours()
        {
            Console.Clear();
            StringBuilder stringBuilder = new StringBuilder();

            Armor armorOne = new("Pig blanket", 1, Slot.Body, ArmorType.Leather, HeroAttributeCreator.CreateAttributes(2, 2, 2));
            Armor armorTwo = new("Breastplate", 1, Slot.Body, ArmorType.Plate, HeroAttributeCreator.CreateAttributes(6, 1, 1));

            Console.WriteLine("Armours:");
            Console.WriteLine("--------------------------------------------\n");
            
            //First armor
            stringBuilder.AppendLine($"Armor: {armorOne.Name}");
            stringBuilder.AppendLine($"Required level: {armorOne.RequiredLevel}");
            stringBuilder.AppendLine($"Required slot: {armorOne.RequiredSlot}");
            stringBuilder.AppendLine($"Type: {armorOne.ArmorType}");
            stringBuilder.AppendLine($"Dexterity: {armorOne.Attributes.Dexterity}");
            stringBuilder.AppendLine($"Strength: {armorOne.Attributes.Strength}");
            stringBuilder.AppendLine($"Intelligence: {armorOne.Attributes.Intelligence}\n");
            //Second armor
            stringBuilder.AppendLine($"Armor: {armorTwo.Name}");
            stringBuilder.AppendLine($"Required level: {armorTwo.RequiredLevel}");
            stringBuilder.AppendLine($"Required slot: {armorTwo.RequiredSlot}");
            stringBuilder.AppendLine($"Type: {armorTwo.ArmorType}");
            stringBuilder.AppendLine($"Dexterity: {armorTwo.Attributes.Dexterity}");
            stringBuilder.AppendLine($"Strength: {armorTwo.Attributes.Strength}");
            stringBuilder.AppendLine($"Intelligence: {armorTwo.Attributes.Intelligence}");

            Console.WriteLine(stringBuilder);
        }
    }
}

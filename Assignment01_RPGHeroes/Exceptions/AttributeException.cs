﻿namespace Assignment01_RPGHeroes.Exceptions
{
    public class AttributeException : Exception
    {
        public AttributeException(string? message) : base(message) { }
        public override string Message => "Cannot calculate attributes";
    }
}

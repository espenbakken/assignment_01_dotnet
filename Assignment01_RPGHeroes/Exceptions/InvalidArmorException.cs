﻿namespace Assignment01_RPGHeroes.Exceptions
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string message) : base(message) { }
        public override string Message => "This armor is not available for you!";
    }
}

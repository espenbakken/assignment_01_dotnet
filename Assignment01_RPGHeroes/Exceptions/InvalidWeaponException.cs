﻿namespace Assignment01_RPGHeroes.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(string message) : base(message) { }
        public override string Message => "This weapon is not available for you";
    }
}

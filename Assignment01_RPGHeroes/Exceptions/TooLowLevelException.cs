﻿namespace Assignment01_RPGHeroes.Exceptions
{
    public class TooLowLevelException : Exception
    {
        public TooLowLevelException(string? message) : base(message) { }
        public override string Message => "Your level is too low!";
    }
}

﻿using Assignment01_RPGHeroes.Main;

namespace Assignment01_RPGHeroes
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to RPG Hero");

            bool isRunning = true;
            while (isRunning)
            {
                isRunning = Display.MainMenu();
            }
        }
    }
}
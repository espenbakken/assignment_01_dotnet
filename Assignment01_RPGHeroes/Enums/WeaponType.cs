﻿namespace Assignment01_RPGHeroes.Enums
{
    public enum WeaponType
    {
        Axes,
        Bows,
        Daggers,
        Hammers,
        Staffs,
        Swords,
        Wand
    }
}

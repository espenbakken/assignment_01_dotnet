﻿namespace Assignment01_RPGHeroes.Enums
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}

﻿namespace Assignment01_RPGHeroes.Enums
{
    public enum Slot
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}

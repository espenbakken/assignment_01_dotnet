﻿namespace Assignment01_RPGHeroes.HeroAttributes
{
    public class HeroAttribute
    {
        //Attributes
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        
        //Constructor for attributes
        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }


        //Methods
        public static HeroAttribute operator +(HeroAttribute old, HeroAttribute newer) => new(old!.Strength + newer.Strength, old.Dexterity + newer.Dexterity, old.Intelligence + newer.Intelligence);

        public override bool Equals(object? obj)
        {
            return obj is HeroAttribute attribute &&
                   Strength == attribute.Strength &&
                   Dexterity == attribute.Dexterity &&
                   Intelligence == attribute.Intelligence;
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
    }
}

﻿namespace Assignment01_RPGHeroes.HeroAttributes
{
    public class HeroAttributeCreator
    {
        //Create Attributes
        public static HeroAttribute CreateAttributes(int strength, int dexterity, int intelligence) => new(strength, dexterity, intelligence);

        //Starting Attributes
        public static HeroAttribute MageStartingAttributes() => new(1, 1, 8);
        public static HeroAttribute RangerStartingAttributes() => new(1, 7, 1);
        public static HeroAttribute RogueStartingAttributes() => new(2, 6, 1);
        public static HeroAttribute WarriorStartingAttributes() => new(5, 2, 1);

        //Attributes when a hero is levelling up
        public static HeroAttribute MageLevelUp() => new(1, 1, 5);
        public static HeroAttribute RangerLevelUp() => new(1, 5, 1);       
        public static HeroAttribute RogueLevelUp() => new(1, 4, 1);
        public static HeroAttribute WarriorLevelUp() => new(3, 2, 1);
    }
}

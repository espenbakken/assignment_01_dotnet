using Assignment01_RPGHeroes.Heroes;
using Assignment01_RPGHeroes.Items;
using Assignment01_RPGHeroes.Enums;
using Assignment01_RPGHeroes.Exceptions;
using Assignment01_RPGHeroes.HeroAttributes;

namespace Assignment01_RPGHeroesTests
{
    public class HeroTest
    {
        /**
         * When a Hero is created, it needs to have the correct name, level, and attributes
         */
        [Fact]
        public void Created_Hero_Has_Correct_Name()
        {
            //Arrange
            string name = "Test hero";
            Hero hero = new Mage(name);
            string expected = name;

            //Act
            string actual = hero.Name;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Created_Hero_Has_Correct_Level()
        {
            //Arrange
            int level = 1;
            string name = "Test hero";
            Hero hero = new Mage(name);
            int expected = level;

            //Act
            int actual = hero.Level;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Created_Hero_Has_Correct_Attributes()
        {
            //Arrange
            int intelligence = 8;
            string name = "Test hero";
            Hero hero = new Mage(name);
            int expected = intelligence;

            //Act
            int actual = hero.BaseStatistics.Intelligence;

            //Assert 
            Assert.Equal(expected, actual);
        }

        /**When a Heroes level is increased, it needs to increment by the correct amount and
         * result in the correct attributes.
         */

        //Ranger
        [Fact]
        public void Ranger_Has_Correct_Levelup()
        {
            //Arrange
            string name = "Test ranger";
            int expected = 2;
            Hero hero = new Ranger(name);
            hero.LevelUp();

            //Act
            int actual = hero.Level;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Ranger_Has_Correct_Levelup_Dexterity()
        {
            //Arrange
            string name = "Test ranger";
            int expected = 12;
            Hero hero = new Ranger(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Dexterity;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Ranger_Has_Correct_Levelup_Strength()
        {
            //Arrange
            string name = "Test ranger";
            int expected = 2;
            Hero hero = new Ranger(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Strength;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Ranger_Has_Correct_Levelup_Intelligence()
        {
            //Arrange
            string name = "Test ranger";
            int expected = 2;
            Hero hero = new Ranger(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Intelligence;

            //Assert 
            Assert.Equal(expected, actual);
        }

        //Mage
        [Fact]
        public void Mage_Has_Correct_Levelup()
        {
            //Arrange
            string name = "Test mage";
            int expected = 2;
            Hero hero = new Mage(name);
            hero.LevelUp();

            //Act
            int actual = hero.Level;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Mage_Has_Correct_Levelup_Dexterity()
        {
            //Arrange
            string name = "Test mage";
            int expected = 2;
            Hero hero = new Mage(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Dexterity;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Mage_Has_Correct_Levelup_Strength()
        {
            //Arrange
            string name = "Test mage";
            int expected = 2;
            Hero hero = new Mage(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Strength;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Mage_Has_Correct_Levelup_Intelligence()
        {
            //Arrange
            string name = "Test mage";
            int expected = 13;
            Hero hero = new Mage(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Intelligence;

            //Assert 
            Assert.Equal(expected, actual);
        }

        //Rogue
        [Fact]
        public void Rogue_Has_Correct_Levelup()
        {
            //Arrange
            string name = "Test rogue";
            int expected = 2;
            Hero hero = new Rogue(name);
            hero.LevelUp();

            //Act
            int actual = hero.Level;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Rogue_Has_Correct_Levelup_Dexterity()
        {
            //Arrange
            string name = "Test rogue";
            int expected = 10;
            Hero hero = new Rogue(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Dexterity;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Rogue_Has_Correct_Levelup_Strength()
        {
            //Arrange
            string name = "Test rogue";
            int expected = 3;
            Hero hero = new Rogue(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Strength;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Rogue_Has_Correct_Levelup_Intelligence()
        {
            //Arrange
            string name = "Test rogue";
            int expected = 2;
            Hero hero = new Rogue(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Intelligence;

            //Assert 
            Assert.Equal(expected, actual);
        }

        //Warrior
        [Fact]
        public void Warrior_Has_Correct_Levelup()
        {
            //Arrange
            string name = "Test warrior";
            int expected = 2;
            Hero hero = new Warrior(name);
            hero.LevelUp();

            //Act
            int actual = hero.Level;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Warrior_Has_Correct_Levelup_Dexterity()
        {
            //Arrange
            string name = "Test warrior";
            int expected = 4;
            Hero hero = new Warrior(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Dexterity;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Warrior_Has_Correct_Levelup_Strength()
        {
            //Arrange
            string name = "Test warrior";
            int expected = 8;
            Hero hero = new Warrior(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Strength;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Warrior_Has_Correct_Levelup_Intelligence()
        {
            //Arrange
            string name = "Test warrior";
            int expected = 2;
            Hero hero = new Warrior(name);
            hero.LevelUp();

            //Act
            int actual = hero.BaseStatistics.Intelligence;

            //Assert 
            Assert.Equal(expected, actual);
        }

        /**
         * When Weapon is created, it needs to have the correct name, 
         * required level, slot, weapon type, and damage
         */

        [Fact]
        public void Weapon_Has_Correct_Name()
        {
            //Arrange
            string name = "Test weapon";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.Swords;
            int damage = 5;
            Weapon weapon = new Weapon(name, requiredLevel, damage, weaponType);

            string expected = name;

            //Act
            string actual = weapon.Name;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Weapon_Has_Correct_RequiredLevel()
        {
            //Arrange
            string name = "Test weapon";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.Swords;
            int damage = 5;
            Weapon weapon = new Weapon(name, requiredLevel, damage, weaponType);

            int expected = requiredLevel;

            //Act
            int actual = weapon.RequiredLevel;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Weapon_Has_Correct_WeaponType()
        {
            //Arrange
            string name = "Test weapon";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.Swords;
            int damage = 5;
            Weapon weapon = new Weapon(name, requiredLevel, damage, weaponType);

            WeaponType expected = weaponType;

            //Act
            WeaponType actual = weapon.Type;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Weapon_Has_Correct_Damage()
        {
            //Arrange
            string name = "Test weapon";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.Swords;
            int damage = 5;
            Weapon weapon = new Weapon(name, requiredLevel, damage, weaponType);

            int expected = damage;

            //Act
            int actual = weapon.Damage;

            //Assert 
            Assert.Equal(expected, actual);
        }

        /**
         * When Armor is created, it needs to have the correct name, 
         * required level, slot, armor type, and armor attributes
         */
        [Fact]
        public void Armor_Has_Correct_Name()
        {
            //Arrange
            string name = "Test armor plate";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Plate;
            int strength = 1;
            int dexterity = 0;
            int intelligence = 0;
            HeroAttribute stats = new(strength, dexterity, intelligence);
            Slot slot = Slot.Body;

            Armor armor = new Armor(name, requiredLevel, slot, armorType, stats);

            string expected = name;

            //Act
            string actual = armor.Name;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Armor_Has_Correct_RequiredLevel()
        {
            //Arrange
            string name = "Test armor plate";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Plate;
            int strength = 1;
            int dexterity = 0;
            int intelligence = 0;
            HeroAttribute stats = new(strength, dexterity, intelligence);
            Slot slot = Slot.Body;

            Armor armor = new Armor(name, requiredLevel, slot, armorType, stats);

            int expected = requiredLevel;

            //Act
            int actual = armor.RequiredLevel;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Armor_Has_Correct_ArmorType()
        {
            //Arrange
            string name = "Test armor plate";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Plate;
            int strength = 1;
            int dexterity = 0;
            int intelligence = 0;
            HeroAttribute stats = new(strength, dexterity, intelligence);
            Slot slot = Slot.Body;

            Armor armor = new Armor(name, requiredLevel, slot, armorType, stats);

            ArmorType expected = armorType;

            //Act
            ArmorType actual = armor.ArmorType;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Armor_Has_Correct_Stats()
        {
            //Arrange
            string name = "Test armor plate";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Plate;
            int strength = 1;
            int dexterity = 0;
            int intelligence = 0;
            HeroAttribute stats = new(strength, dexterity, intelligence);
            Slot slot = Slot.Body;

            Armor armor = new Armor(name, requiredLevel, slot, armorType, stats);

            HeroAttribute expected = stats;

            //Act
            HeroAttribute actual = armor.Attributes;

            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Armor_Has_Correct_Slot()
        {
            //Arrange
            string name = "Test armor plate";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Plate;
            int strength = 1;
            int dexterity = 0;
            int intelligence = 0;
            HeroAttribute stats = new(strength, dexterity, intelligence);
            Slot slot = Slot.Body;

            Armor armor = new Armor(name, requiredLevel, slot, armorType, stats);

            Slot expected = slot;

            //Act
            Slot actual = armor.RequiredSlot;

            //Assert 
            Assert.Equal(expected, actual);
        }

        /**
         * A Hero should be able to equip a Weapon, the appropriate exceptions
         * should be thrown if invalid (level requirement and type)
         */
        [Fact]
        public void A_Hero_Should_Be_Able_To_Equip_Weapon()
        {
            //Arrange
            string name = "Test hero";
            string nameOfWeapon = "Epic Sword";
            int requiredLevel = 1;
            WeaponType weaponType = WeaponType.Swords;
            int damage = 5;

            Weapon weapon = new Weapon(nameOfWeapon, requiredLevel, damage, weaponType);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Weapon, weapon);

            Hero hero = new Rogue(name);
            hero.EquipWeapon(weapon);

            //Act
            var actual = hero.Inventory;

            //Assert 
            Assert.Equal(expected[Slot.Weapon], actual[Slot.Weapon]);
        }
        [Fact]
        public void A_Hero_Should_Not_Be_Able_To_Equip_Weapon_When_Level_To_Low()
        {
            //Arrange
            string name = "Test hero";
            string nameOfWeapon = "Even better Sword";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.Swords;
            int damage = 5;

            Weapon weapon = new Weapon(nameOfWeapon, requiredLevel, damage, weaponType);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Weapon, weapon);

            Hero hero = new Rogue(name);

            //Act
            var actual = hero.Inventory;

            //Assert 
            Assert.Throws<TooLowLevelException>(() => hero.EquipWeapon(weapon));
        }
        [Fact]
        public void A_Hero_Should_Not_Be_Able_To_Equip_Weapon_When_Wrong_WeaponType()
        {
            //Arrange
            string name = "Test hero";
            string nameOfWeapon = "Staff";
            int requiredLevel = 1;
            WeaponType weaponType = WeaponType.Staffs;
            int damage = 5;

            Weapon weapon = new Weapon(nameOfWeapon, requiredLevel, damage, weaponType);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Weapon, weapon);

            Hero hero = new Rogue(name);

            //Act
            var actual = hero.Inventory;

            //Assert 
            Assert.Throws<InvalidWeaponException>(() => hero.EquipWeapon(weapon));
        }
        /**
         * A Hero should be able to equip Armor, the appropriate exceptions should be 
         * thrown if invalid (level requirement and type)
         */
        [Fact]
        public void A_Hero_Should_Be_Able_To_Equip_Armor()
        {
            //Arrange
            string name = "Test hero";
            string nameOfArmor = "Epic Armor";
            int requiredLevel = 1;
            ArmorType armorType = ArmorType.Mail;
            int strength = 1;
            int dexterity = 1;
            int intelligence = 1;
            HeroAttribute stats = new(strength, dexterity, intelligence);
            Slot slot = Slot.Body;

            Armor armor = new Armor(nameOfArmor, requiredLevel, slot, armorType, stats);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Body, armor);

            Hero hero = new Rogue(name);
            hero.EquipArmor(armor);

            //Act
            var actual = hero.Inventory;

            //Assert 
            Assert.Equal(expected[Slot.Body], actual[Slot.Body]);
        }
        [Fact]
        public void A_Hero_Should_Not_Be_Able_To_Equip_Armor_When_Level_Too_Low()
        {
            //Arrange
            string name = "Test hero";
            string nameOfArmor = "Epic Armor";
            int requiredLevel = 2;
            ArmorType armorType = ArmorType.Mail;
            int strength = 1;
            int dexterity = 1;
            int intelligence = 1;
            HeroAttribute stats = new(strength, dexterity, intelligence);
            Slot slot = Slot.Body;

            Armor armor = new Armor(nameOfArmor, requiredLevel, slot, armorType, stats);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Body, armor);

            Hero hero = new Rogue(name);
            

            //Act
            var actual = hero.Inventory;

            //Assert 
            Assert.Throws<TooLowLevelException>(() => hero.EquipArmor(armor));
        }
        [Fact]
        public void A_Hero_Should_Not_Be_Able_To_Equip_Armor_When_Wrong_ArmorType()
        {
            //Arrange
            string name = "Test hero";
            string nameOfArmor = "Epic Armor 2";
            int requiredLevel = 1;
            ArmorType armorType = ArmorType.Cloth;
            int strength = 1;
            int dexterity = 1;
            int intelligence = 1;
            HeroAttribute stats = new(strength, dexterity, intelligence);
            Slot slot = Slot.Body;

            Armor armor = new Armor(nameOfArmor, requiredLevel, slot, armorType, stats);

            Dictionary<Slot, Item> expected = new Dictionary<Slot, Item>();
            expected.Add(Slot.Body, armor);

            Hero hero = new Rogue(name);


            //Act
            var actual = hero.Inventory;

            //Assert 
            Assert.Throws<InvalidArmorException>(() => hero.EquipArmor(armor));
        }

        /**
         * Total attributes should be calculated correctly
         */
        [Fact]
        public void Attributes_Should_Be_Calculated_Correctly_No_Equipment()
        {
            //Arrange
            string name = "Test hero";
            int strength = 1;
            int dexterity = 1;
            int intelligence = 8;
            HeroAttribute expected = new(strength, dexterity, intelligence);
            Hero hero = new Mage(name);

            //Act
            HeroAttribute actual = hero.BaseStatistics;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Attributes_Should_Be_Calculated_Correctly_With_Equipment()
        {
            //Arrange
            string name = "Test hero";
            int strength = 1;
            int dexterity = 1;
            int intelligence = 8;
            Hero hero = new Mage(name);

            string nameOfArmor = "Test armor";
            int reqLev = 1;
            ArmorType armorType = ArmorType.Cloth;
            int aStrength = 1;
            int aDexterity = 0;
            int aIntelligence = 0;
            HeroAttribute stats = new(aStrength, aDexterity, aIntelligence);
            Slot slot = Slot.Body;
            Armor armor = new Armor(nameOfArmor, reqLev, slot, armorType, stats);

            HeroAttribute heroAttribute = new(strength, dexterity, intelligence);
            HeroAttribute expected = heroAttribute + stats;

            hero.EquipArmor(armor);

            HeroAttribute actual = hero.CalcTotalAttr();
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Attributes_Should_Be_Calculated_Correctly_With_Two_Equipments()
        {
            //Arrange
            string name = "Test hero";
            int strength = 1;
            int dexterity = 1;
            int intelligence = 8;
            Hero hero = new Mage(name);

            string nameOfArmor = "Test armor";
            int reqLev = 1;
            ArmorType armorType = ArmorType.Cloth;
            int aStrength = 1;
            int aDexterity = 0;
            int aIntelligence = 0;
            HeroAttribute stats = new(aStrength, aDexterity, aIntelligence);
            Slot slot = Slot.Body;
            Armor armor = new Armor(nameOfArmor, reqLev, slot, armorType, stats);
           
            string nameOfArmorLegs = "Test armor legs";
            int reqLevLegs = 1;
            ArmorType armorTypeLegs = ArmorType.Cloth;
            int aStrength2 = 1;
            int aDexterity2 = 0;
            int aIntelligence2 = 0;
            HeroAttribute statsLegs = new(aStrength2, aDexterity2, aIntelligence2);
            Slot slot2 = Slot.Legs;
            Armor armor2 = new Armor(nameOfArmorLegs, reqLevLegs, slot2, armorTypeLegs, statsLegs);

            HeroAttribute heroAttribute = new(strength, dexterity, intelligence);
            HeroAttribute expected = heroAttribute + stats + statsLegs;

            hero.EquipArmor(armor);
            hero.EquipArmor(armor2);

            HeroAttribute actual = hero.CalcTotalAttr();
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Attributes_Should_Be_Calculated_Correctly_When_Same_Slot()
        {
            //Arrange
            string name = "Test hero";
            int strength = 1;
            int dexterity = 1;
            int intelligence = 8;
            Hero hero = new Mage(name);

            string nameOfArmor = "Test armor";
            int reqLev = 1;
            ArmorType armorType = ArmorType.Cloth;
            int aStrength = 1;
            int aDexterity = 0;
            int aIntelligence = 0;
            HeroAttribute stats = new(aStrength, aDexterity, aIntelligence);
            Slot slot = Slot.Body;
            Armor armor = new Armor(nameOfArmor, reqLev, slot, armorType, stats);

            string nameOfArmorLegs = "Test armor legs";
            int reqLevLegs = 1;
            ArmorType armorTypeLegs = ArmorType.Cloth;
            int aStrength2 = 1;
            int aDexterity2 = 0;
            int aIntelligence2 = 0;
            HeroAttribute statsLegs = new(aStrength2, aDexterity2, aIntelligence2);
            Slot slot2 = Slot.Body;
            Armor armor2 = new Armor(nameOfArmorLegs, reqLevLegs, slot2, armorTypeLegs, statsLegs);

            HeroAttribute heroAttribute = new(strength, dexterity, intelligence);
            HeroAttribute expected = heroAttribute + statsLegs;

            hero.EquipArmor(armor);
            hero.EquipArmor(armor2);

            HeroAttribute actual = hero.CalcTotalAttr();
            //Assert
            Assert.Equal(expected, actual);
        }

        /**
        * Hero damage should be calculated properly
        */
        [Fact]
        public void Damage_Should_Be_Calculated_Correctly_With_No_Weapon()
        {
            string name = "Test hero";
            Hero hero = new Warrior(name);

            double expected = 1;

            double actual = hero.Dmg;

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Damage_Should_Be_Calculated_Correctly_With_Weapon()
        {
            string name = "Test hero";
            Hero hero = new Warrior(name);

            string nameOfWeapon = "Epic wand";
            int reqLev = 1;
            WeaponType wand = WeaponType.Wand;
            int weaponDmg = 4;
            Weapon weapon = new Weapon(nameOfWeapon, reqLev, weaponDmg, wand);

            hero.EquipWeapon(weapon);

            double expected = 4;
            double actual = hero.damage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Damage_Should_Be_Calculated_Correctly_With_Replaced_Weapon()
        {
            string name = "Test hero";
            Hero hero = new Warrior(name);

            string nameOfWeapon = "Epic wand";
            int reqLev = 1;
            WeaponType wand = WeaponType.Wand;
            int weaponDmg = 2;
            Weapon weapon = new Weapon(nameOfWeapon, reqLev, weaponDmg, wand);

            string nameOfWeapon2 = "Better wand";
            WeaponType wand2 = WeaponType.Wand;
            int weaponDmg2 = 4;
            Weapon weapon2 = new Weapon(nameOfWeapon2, reqLev, weaponDmg2, wand2);

            hero.EquipWeapon(weapon);
            hero.EquipWeapon(weapon2);

            double expected = 4;
            double actual = hero.damage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Damage_Should_Be_Calculated_Correctly_With_Armor_And_Weapon()
        {
            string name = "Test hero";
            Hero hero = new Warrior(name);

            string nameOfWeapon = "Epic wand";
            int reqLev = 1;
            WeaponType wand = WeaponType.Wand;
            int weaponDmg = 2;
            Weapon weapon = new Weapon(nameOfWeapon, reqLev, weaponDmg, wand);

            string armorName = "Test armor";
            ArmorType armorType = ArmorType.Mail;
            int strength = 1;
            int dexterity = 0;
            int intelligence = 0;
            HeroAttribute stats = new(strength, dexterity, intelligence);
            Slot slot = Slot.Body;

            Armor armor = new Armor(armorName, reqLev, slot, armorType, stats);
            Dictionary<Slot, Item> inventory = new Dictionary<Slot, Item>();
            inventory.Add(Slot.Body, armor);

            hero.EquipWeapon(weapon);
            hero.EquipArmor(armor);

            double expected = 2;
            double actual = hero.damage();

            Assert.Equal(expected, actual);
        }
    }

}
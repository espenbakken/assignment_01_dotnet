## Name
Assignment 01 - Dotnet 
RPG Heroes

## Description
This project is an console application where you can create heroes, weapons and armors. The project consists of appropriate tests to test the functionality. 


## Insallation
Clone this project. Make sure you have .NET 6 installed. 

## Usage
This Project has a dummy run build. You can try it out with Run-command in your IDE, or by using:
```
dotnet build --no-restore
```
in the terminal.

Project primarily uses the test suite to "run" the program, try it out with "Run all tests", or by using:
```
dotnet test --no-restore
```
in the terminal.

## License
Use as you wish in your own repository. Please do not override excisting repo. 